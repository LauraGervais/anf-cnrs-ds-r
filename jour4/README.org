#+TITLE: Programme du 25/11/2021

En groupes séparés ; on décrit ici le parcours R uniquement.

** Outils pour la science reproductible avec R
- Exposé des différents niveaux de reproductibilité atteignables avec R, et des outils à mettre en oeuvre
- Démonstration de deux alternatives à Jupyter : [[https://orgmode.org/][Org mode]] et [[https://rmarkdown.rstudio.com/][Rmarkdown]] ; comparaison des trois outils
- Démonstration d'utilisation d'images Docker pour réaliser des articles totalement reproductibles

** Exercice pratique
- Création d'un nouveau dépôt GitUB vierge
- Transformation de ce dépôt en un dépôt /Binder/

** Bilan global de la formation
- Résumé des connaissances acquises et séance de questions / réponses
- Préparation de la session de restitution pour la cinquième et dernière matinée
