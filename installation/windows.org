#+TITLE: Instructions d'installation pour systèmes Windows

** Éditeur de texte
Un éditeur de texte avancé pourra être utile à certains moments de la formation. Pour Windows, [[https://notepad-plus-plus.org/downloads/][Notepad++]] est un bon choix.

Naturellement, si vous êtes déjà familier avec un autre éditeur avancé (Atom, Emacs, Sublime Text, ...), cela conviendra très bien également.

** Git
Git sera utilisé tout au long de la formation comme outil de versionnement avancé. Vous pouvez installer la version Windows depuis [[https://git-scm.com/download/win][le site officiel]].
Au cours du processus d'installation, de nombreuses options vous seront proposées : vous pouvez à chaque étape laisser le choix par défaut et cliquer sur "Next" (à l'exception peut-être de l'étape vous demandant de choisir l'éditeur de texte par défaut utilisé par Git : choisissez alors Notepad++, ou sinon tout autre éditeur que vous utilisez habituellement).

** Anaconda
Anaconda est une distribution complète incluant de nombreux logiciels et packages utiles en science des données. Vous pouvez télécharger et installer cette distribution [[https://www.anaconda.com/products/individual#Downloads][depuis le site officiel]].

** Plug-in ~jupyter-git~
Afin de pouvoir interagir aisément avec Git à travers l'interface JupyterLab que nous utiliserons pendant la formation, un plugin additionnel sera nécessaire. Pour l'installer :
1. Ouvrir Anaconda Prompt (par exemple via votre menu Démarrer).
2. Copier-coller puis exécuter dans cette console la commande suivante :
   #+begin_src shell :eval no :exports code
conda install -c conda-forge jupyterlab-git
   #+end_src

** Manipulation supplémentaire uniquement pour les stagiaires suivant le parcours R
Nous supposons que les stagiaires qui suivent le parcours R ont déjà le logiciel R installé sur leur poste (si possible la dernière version disponible ; en tout cas une version supérieure ou égale à la 4.1.1).

Avant tout, assurez-vous que R figure bien dans votre PATH. Par exemple, vous pourrez trouver utile [[https://cran.r-project.org/bin/windows/base/rw-FAQ.html#Rcmd-is-not-found-in-my-PATH_0021][cette entrée de la FAQ officielle de R]]. Puis :

1. Ouvrir Anaconda Prompt (par exemple via votre menu Démarrer).
2. Dans la console qui s'ouvre, taper `R` puis valider par Entrée pour ouvrir une console R dans l'Anaconda Prompt.
3. Une fois la session R ouverte, exécuter les deux instructions suivantes :
   #+begin_src R :exports code :eval no
install.packages("IRkernel")
IRkernel::installspec()
   #+end_src
4. Une fois l'installation réussie, vous pouvez fermer le tout.

** Vérifier l'installation
Pour vous assurer que tout l'environnement Jupyter a été correctement installé, vous pouvez effectuer les vérifications conseillées sur [[https://docs.anaconda.com/anaconda/install/verify-install/][le site officiel d'Anaconda]].

De plus, en particulier si vous suivez le parcours R, il peut être conseillé d'essayer d'ouvrir Anaconda Navigator, puis de lancer JupyterLab. Vous devriez alors, après un court temps de chargement, obtenir une fenêtre ressemblant à ceci dans votre navigateur préféré :
[[./images/accueil_juplab.png]]

Vous pouvez éventuellement avoir moins de langages disponibles dans la section "Notebook", mais assurez-vous au moins que l'icône représentant votre langage de choix, R ou Python, y est bien présente. De plus, assurez-vous que l'icône Git (entourée en rouge sur la figure ci-dessus) est bien présente ; dans le cas contraire, re-tentez l'installation du plug-in ~jupyter-git~.
